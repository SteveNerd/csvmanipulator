﻿using System;
using Donoghue.CSVBatchProcessor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;


namespace CSVBatchProcessor_Test
{
    [TestClass]
    public class UnitTest1
    {
        public static string GetSourceDirectory()
        {
            string executing_dir = AppDomain.CurrentDomain.BaseDirectory;
            string parent_parent_dir = Path.GetDirectoryName(Path.GetDirectoryName(executing_dir));
            string source_dir = Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName(executing_dir)), "SourceTestingFiles");
            return source_dir;
        }
        public static string GetActualDirectory()
        {
            string executing_dir = AppDomain.CurrentDomain.BaseDirectory;
            string parent_parent_dir = Path.GetDirectoryName(Path.GetDirectoryName(executing_dir));
            string actual_dir = Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName(executing_dir)), "ActualTestingFiles");
            return actual_dir;
        }
        public static string GetExpectedDirectory()
        {
            string executing_dir = AppDomain.CurrentDomain.BaseDirectory;
            string parent_parent_dir = Path.GetDirectoryName(Path.GetDirectoryName(executing_dir));
            string expected_dir = Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName(executing_dir)), "ExpectedTestingFiles");
            return expected_dir; 

        }

        public static string AssemblyDirectory()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            return dir;
        }

        public void SetUp()
        {

        }

        [TestMethod]
        public void GetFullArguments()
        {
            string argument = @"/path=mypath /array=0,2,4 /replace=this /with=that /case-sensitive=true /delimiter=, /filter=*.filt /prefix=before";
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Assert.AreEqual("mypath", argu.Path);
            Assert.AreEqual("this", argu.Replace);
            Assert.AreEqual("that", argu.With);
            Assert.IsTrue(argu.ReplaceWith);
            Assert.AreEqual(',', argu.Delimiter);
            CollectionAssert.AreEqual(new int[] { 0, 2, 4 }, argu.TargetColumnConfiguration);
            Assert.AreEqual("*.filt", argu.Filter);
            Assert.AreEqual("before", argu.RenamePrefix);
            Assert.IsTrue(argu.CaseSensitive);
        }

        [TestMethod]
        public void GetPartialArguments()
        {
            string argument = @"/path=somepath /array=1,5,3";
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Assert.AreEqual("somepath", argu.Path);
            Assert.IsFalse(argu.ReplaceWith);
            Assert.AreEqual(',', argu.Delimiter);
            CollectionAssert.AreEqual(new int[] { 1,5,3}, argu.TargetColumnConfiguration);
            Assert.AreEqual("*.csv", argu.Filter);
            Assert.AreEqual("new_", argu.RenamePrefix);
            Assert.IsFalse(argu.CaseSensitive);
        }

        [TestMethod]
        public void TestSubstitutionBody()
        {
            File.Copy(Path.Combine(GetSourceDirectory(), "toilets.csv"), Path.Combine(GetActualDirectory(), "toilets.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2,3,4,5,6,7,8,9 /replace=Baby /with=Horse /delimiter=, /filter=toilets.csv /prefix=after_horse_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("after_horse_toilets*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            string expected_file = expected_dir.GetFiles("toilets-horse.csv")[0].FullName;
            string expected_string = File.ReadAllText(expected_file);

            Assert.AreEqual(expected_string, actual_string);

        }
        [TestMethod]
        public void TestSubstitutionHeader()
        {
            File.Copy(Path.Combine(GetSourceDirectory(), "toilets.csv"), Path.Combine(GetActualDirectory(), "toilets.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2,3,4,5,6,7,8,9 /replace=Male /with=Gents /delimiter=, /filter=toilets.csv /prefix=after_gents_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("after_gents_toilets*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            string expected_file = expected_dir.GetFiles("toilets-gents-insensitive.csv")[0].FullName;
            string expected_string = File.ReadAllText(expected_file);

            Assert.AreEqual(expected_string, actual_string);

        }

        [TestMethod]
        public void TestSubstitutionHeaderCaseSensitive()
        {
            File.Copy(Path.Combine(GetSourceDirectory(), "toilets.csv"), Path.Combine(GetActualDirectory(), "toilets.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2,3,4,5,6,7,8,9 /replace=Male /with=Gents /case-sensitive=True /delimiter=, /filter=toilets.csv /prefix=after_gents_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("after_gents_toilets*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            string expected_file = expected_dir.GetFiles("toilets-gents-sensitive.csv")[0].FullName;
            string expected_string = File.ReadAllText(expected_file);

            Assert.AreEqual(expected_string, actual_string);

        }

        [TestMethod]
        public void TestRemoveColumn()
        {
            File.Copy(Path.Combine(GetSourceDirectory(), "NPSBats.csv"), Path.Combine(GetActualDirectory(), "NPSBats.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,2 /delimiter=, /filter=NPSBats.csv /prefix=1_3_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("1_3_*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            string expected_file = expected_dir.GetFiles("13NPSBats.csv")[0].FullName;
            string expected_string = File.ReadAllText(expected_file);

            Assert.AreEqual(expected_string, actual_string);
        }

        [TestMethod]
        public void TestReorderColumns()
        {

            File.Copy(Path.Combine(GetSourceDirectory(), "NPSBats.csv"), Path.Combine(GetActualDirectory(), "NPSBats.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,3,1,2 /delimiter=, /filter=NPSBats.csv /prefix=1_4_2_3_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("1_4_2_3_*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            string expected_file = expected_dir.GetFiles("1423NPSBats.csv")[0].FullName;
            string expected_string = File.ReadAllText(expected_file);

            Assert.AreEqual(expected_string, actual_string);
        }

        [TestMethod]
        public void DifferentDelimiter()
        {

            File.Copy(Path.Combine(GetSourceDirectory(), "download.csv"), Path.Combine(GetActualDirectory(), "download.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2 /delimiter=; /replace=S /with=ff /case-sensitive=True /filter=download.csv /prefix=semi_colon_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("semi_colon_*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            //DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            //string expected_file = expected_dir.GetFiles("1423NPSBats.csv")[0].FullName;
            //string expected_string = File.ReadAllText(expected_file);
        }

        [TestMethod]
        public void SurviveAndPadIfLackingColumns()
        {

            File.Copy(Path.Combine(GetSourceDirectory(), "toilets.csv"), Path.Combine(GetActualDirectory(), "toilets.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19 /delimiter=, /filter=toilets.csv /prefix=now_padded_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("now_padded_*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            string expected_file = expected_dir.GetFiles("toilets-padded.csv")[0].FullName;
            string expected_string = File.ReadAllText(expected_file);

            Assert.AreEqual(expected_string, actual_string);
        }

        [TestMethod]
        public void FileIntactIfNothingToDo()
        {

            File.Copy(Path.Combine(GetSourceDirectory(), "toilets.csv"), Path.Combine(GetActualDirectory(), "toilets.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2,3,4,5,6,7,8,9 /delimiter=, /filter=toilets.csv /prefix=unaltered_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("unaltered_*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            DirectoryInfo expected_dir = new DirectoryInfo(GetExpectedDirectory());
            string expected_file = expected_dir.GetFiles("toilets-unaltered.csv")[0].FullName;
            string expected_string = File.ReadAllText(expected_file);

            Assert.AreEqual(expected_string, actual_string);
        }
        [TestMethod]
        public void SurviveIfJunkInArray()
        {

            File.Copy(Path.Combine(GetSourceDirectory(), "toilets.csv"), Path.Combine(GetActualDirectory(), "toilets.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2,3,4,5,6,7,8,9, /delimiter=, /filter=toilets.csv /prefix=unaltered_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');
            Assert.IsTrue(true);

        }



        [TestMethod]
        public void SystematicRename()
        {
            File.Copy(Path.Combine(GetSourceDirectory(), "toilets.csv"), Path.Combine(GetActualDirectory(), "toilets.csv"), true);

            string argument = string.Format(@"/path={0} /array=0,1,2,3,4,5,6,7,8,9 /replace=Male /with=Gents /delimiter=, /filter=toilets.csv /prefix=systematic_rename_", GetActualDirectory());
            string[] argument_array = argument.Split(' ');

            Arguments argu = new Arguments(argument_array);

            Program.RunManipulator(argu);

            DirectoryInfo actual_dir = new DirectoryInfo(GetActualDirectory());
            string actual_file = actual_dir.GetDirectories("output")[0].GetFiles("systematic_rename_*.csv")[0].FullName;
            string actual_string = File.ReadAllText(actual_file);

            Assert.IsNotNull(actual_string);

        }



    }
}
