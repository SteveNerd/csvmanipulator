﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donoghue.CSVBatchProcessor
{
    public static class StringExtensions
    {

        public static string ComparisonReplace(this string original , string find, string replace, StringComparison comparison_type)
        {
            StringBuilder sb = new StringBuilder();

            int previousIndex = 0;
            int index = original.IndexOf(find, comparison_type);
            while (index != -1)
            {
                sb.Append(original.Substring(previousIndex, index - previousIndex));
                sb.Append(replace);
                index += find.Length;

                previousIndex = index;
                index = original.IndexOf(find, index, comparison_type);
            }
            sb.Append(original.Substring(previousIndex));

            //original = sb.ToString();
            return sb.ToString();
        }

//public static string ReplaceString(string str, string oldValue, string newValue, StringComparison comparison)
//        {
//            StringBuilder sb = new StringBuilder();

//            int previousIndex = 0;
//            int index = str.IndexOf(oldValue, comparison);
//            while (index != -1)
//            {
//                sb.Append(str.Substring(previousIndex, index - previousIndex));
//                sb.Append(newValue);
//                index += oldValue.Length;

//                previousIndex = index;
//                index = str.IndexOf(oldValue, index, comparison);
//            }
//            sb.Append(str.Substring(previousIndex));

//            return sb.ToString();
//        }
    }
}
