﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donoghue.CSVBatchProcessor
{
    public class Arguments
    {
        /// <summary>
        /// Directory to operate on 
        /// Defaults to application directory
        /// </summary>
        public string Path { get; private set; } = AppDomain.CurrentDomain.BaseDirectory;
        /// <summary>
        /// File name filter
        /// Defaults to *.csv
        /// </summary>
        public string Filter { get; private set; } = "*.csv";
        /// <summary>
        /// Find replace functionality is enabled by supplying 'replace' and 'with' arguments
        /// </summary>
        public bool ReplaceWith { get; private set; } = false;
        /// <summary>
        /// String to find in file
        /// </summary>
        public string Replace { get; private set; } = null;
        /// <summary>
        /// String that replaces the find string
        /// </summary>
        public string With { get; private set; } = null;
        /// <summary>
        /// Defaults to comma
        /// </summary>
        public char Delimiter { get; private set; } = ',';
        /// <summary>
        /// The desired new order
        /// </summary>
        public int[] TargetColumnConfiguration { get; private set; }
        /// <summary>
        /// new files will be created with this prefix
        /// defaults to 'new_'
        /// </summary>
        public string RenamePrefix { get; set; } = "new_";
        /// <summary>
        /// 
        /// </summary>
        public bool CaseSensitive { get; set; } = false;

        public Arguments(string[] args)
        {
            bool replaceset = false;
            bool withset = false;
            foreach (string argument in args)
            {
                try
                {
                    if (argument.StartsWith("/path="))
                    {
                        Path = argument.Split('=')[1];
                        //directory = new DirectoryInfo(path);
                    }
                    else if (argument.StartsWith("/array="))
                    {
                        string column_list = argument.Split('=')[1];
                        string[] str_arry_targets = column_list.Split(',');

                        // convert string[] to int[]
                        TargetColumnConfiguration = Array.ConvertAll(str_arry_targets, int.Parse);

                        //TargetColumnConfiguration = new int[str_arry_targets.Length];
                        //for (int i = 0; i < str_arry_targets.Length; i++)
                        //{
                        //    int i_target = 0;
                        //    if (int.TryParse(str_arry_targets[i], out i_target))
                        //    {
                        //        TargetColumnConfiguration[i] = i_target;
                        //    }
                        //    else
                        //    {
                        //        throw new ArgumentException(string.Format("{0}: is not a valid column index", str_arry_targets[i]));
                        //    }
                        //}
                    }
                    else if (argument.StartsWith("/replace="))
                    {
                        Replace = argument.Split('=')[1];
                        replaceset = true;
                    }
                    else if (argument.StartsWith("/with="))
                    {
                        With = argument.Split('=')[1];
                        withset = true;
                    }
                    else if (argument.StartsWith("/delimiter="))
                    {
                        // take the first char
                        Delimiter = argument.Split('=')[1][0];
                    }
                    else if (argument.StartsWith("/filter="))
                    {
                        // take the first char
                        Filter = argument.Split('=')[1];
                    }
                    else if (argument.StartsWith("/prefix="))
                    {
                        // take the first char
                        RenamePrefix = argument.Split('=')[1];
                    }
                    else if (argument.StartsWith("/case-sensitive="))
                    {
                        // take the first char
                        CaseSensitive = Boolean.Parse(argument.Split('=')[1]);
                    }
                    else
                    {
                        throw new ArgumentException(string.Format("{0} is not a valid argument", argument));
                    }
                }
                catch (FormatException)
                {
                    throw new ArgumentException(string.Format("Something wrong with {0}", argument));
                }
            }
            // turn on replace with functionality if both with and replace are set ;
            ReplaceWith = (replaceset && withset);
        }
    }
}
