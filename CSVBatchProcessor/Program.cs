﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donoghue.CSVBatchProcessor
{
    public class Program
    {
        static void Main(string[] args)
        {
            Arguments arguments = null;
            try
            {
                arguments = new Arguments(args);
            }
            catch (FormatException ex)
            {
                Console.Write(ex.Message);
            }
            RunManipulator(arguments); 
        }

        public static void RunManipulator(Arguments arguments )
        {
            // get files to work on 
            DirectoryInfo directory = new DirectoryInfo(arguments.Path);
            foreach (FileInfo old_file in directory.GetFiles(arguments.Filter))
            {
                // generate the contents of the new file 
                string new_contents = ProcessFileContents(old_file, arguments);
                
                // decide what to call the new file
                string new_file_name = Path.Combine(Path.GetDirectoryName(old_file.FullName), "output", !string.IsNullOrWhiteSpace(arguments.RenamePrefix)?arguments.RenamePrefix+old_file.Name:old_file.Name );
                // write the new file back to disk
                using (StreamWriter new_file_writer = new StreamWriter(new_file_name))
                {
                    new_file_writer.Write(new_contents);
                }
            }
        }

        public static string ProcessFileContents(FileInfo source_file, Arguments arguments)
        {
            string[] lines = File.ReadAllLines(source_file.FullName);
            string[] replaced_lines = lines; 
            if (arguments.ReplaceWith)
            {
                if (arguments.CaseSensitive)
                {
                    for (int l = 0; l < replaced_lines.Length; l++)
                    {
                        replaced_lines[l] = lines[l].Replace(arguments.Replace, arguments.With);
                    }
                }
                else
                {
                    for (int l = 0; l < replaced_lines.Length; l++)
                    {
                        replaced_lines[l] = lines[l].ComparisonReplace(arguments.Replace, arguments.With, StringComparison.InvariantCultureIgnoreCase);
                    }
                }
            }
            string[] reordered_lines = replaced_lines;
            for(int l = 0; l< reordered_lines.Length; l++)
            {
                string[] broken_line = reordered_lines[l].Split(arguments.Delimiter);
                StringBuilder reassembled_line = new StringBuilder();
                foreach (int included_column in arguments.TargetColumnConfiguration)
                {
                    try
                    {
                        reassembled_line.Append(broken_line[included_column]);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        // if requested column is not present just carry on
                    }
                    reassembled_line.Append(arguments.Delimiter);
                }


                string finished_new_line=  reassembled_line.ToString().Substring(0, reassembled_line.Length - 1);
                reordered_lines[l] = string.Concat(finished_new_line, "\r\n");
            }
            //MemoryStream new_contents = new MemoryStream();
            //StreamWriter sw = new StreamWriter(new_contents);
            StringBuilder new_contents = new StringBuilder();
            foreach (string reordered_line in reordered_lines)
            {
                new_contents.Append(reordered_line); 
                //sw.Write(reordered_line);
            }
            //new_contents.Seek(0, SeekOrigin.Begin);
            return new_contents.ToString();
        }
    }
}
